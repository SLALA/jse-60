package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    AbstractListener getCommandByName(@NotNull String name);

    @NotNull
    AbstractListener getCommandByArg(@NotNull String arg);

}
