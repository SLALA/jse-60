package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.api.service.ILoggerService;
import ru.t1.strelcov.tm.component.FileScanner;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.SystemUtil;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Getter
public final class Bootstrap {

    @Autowired
    @NotNull
    private ICommandService commandService;

    @Autowired
    @NotNull
    private ILoggerService loggerService;

    @Autowired
    @NotNull
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(@Nullable String... args) {
        initPID();
        displayWelcome();
        fileScanner.init();
        try {
            if (parseArgs(args))
                System.exit(0);
        } catch (Exception e) {
            loggerService.errors(e);
            System.exit(0);
        }
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                parseCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (Throwable e) {
                loggerService.errors(e);
                System.err.println("[FAIL]");
            } finally {
                System.out.println();
            }
        }
    }

    public void displayWelcome() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null) return;
        loggerService.commands(arg);
        publisher.publishEvent(new ConsoleEvent(arg));
    }

    public void parseCommand(@Nullable final String command) {
        if (command == null) return;
        loggerService.commands(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

}
