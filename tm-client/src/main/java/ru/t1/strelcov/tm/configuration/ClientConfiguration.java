package ru.t1.strelcov.tm.configuration;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.strelcov.tm.api.endpoint.*;
import ru.t1.strelcov.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.strelcov.tm")
public class ClientConfiguration {

    @Bean
    public IAuthEndpoint authEndpoint(@NotNull final IPropertyService propertyService) {
        return IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public ISystemEndpoint systemEndpoint(@NotNull final IPropertyService propertyService) {
        return ISystemEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public IUserEndpoint userEndpoint(@NotNull final IPropertyService propertyService) {
        return IUserEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public IProjectEndpoint projectEndpoint(@NotNull final IPropertyService propertyService) {
        return IProjectEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public ITaskEndpoint taskEndpoint(@NotNull final IPropertyService propertyService) {
        return ITaskEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @Bean
    public IDataEndpoint dataEndpoint(@NotNull final IPropertyService propertyService) {
        return IDataEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());
    }

}
