package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error: Unknown argument.");
    }

}
