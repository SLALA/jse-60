package ru.t1.strelcov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.DataXmlFasterXMLLoadRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXMLListener extends AbstractDataListener {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-xml-load-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from xml file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA XML LOAD]");
        dataEndpoint.loadXmlFasterXMLData(new DataXmlFasterXMLLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
