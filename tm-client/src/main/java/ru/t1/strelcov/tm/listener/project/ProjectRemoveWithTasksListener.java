package ru.t1.strelcov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectRemoveWithTasksRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveWithTasksListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with bound tasks.";
    }

    @Override
    @EventListener(condition = "@projectRemoveWithTasksListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT WITH PROJECTS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.removeWithTasksProject(new ProjectRemoveWithTasksRequest(getToken(), id)).getProject();
    }

}
