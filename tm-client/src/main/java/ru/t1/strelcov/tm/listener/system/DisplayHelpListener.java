package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class DisplayHelpListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    @EventListener(condition = "@displayHelpListener.name() == #event.name || @displayHelpListener.arg() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        final Collection<AbstractListener> commands = commandService.getCommands();
        for (final AbstractListener command : commands) {
            System.out.println(command);
        }
    }

}
