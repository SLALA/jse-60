package ru.t1.strelcov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserListRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.event.ConsoleEvent;

import java.util.List;

@Component
public final class UserListListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List users.";
    }

    @Override
    @EventListener(condition = "@userListListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LIST USERS]");
        @NotNull final List<UserDTO> users = userEndpoint.listUser(new UserListRequest(getToken())).getList();
        int index = 1;
        for (final UserDTO user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
