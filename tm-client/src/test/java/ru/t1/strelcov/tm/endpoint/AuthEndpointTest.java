package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.configuration.ClientConfiguration;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private static final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @Before
    public void before() {
    }

    @After
    public void after() {
    }

    @Test
    public void loginTest() {
        Assert.assertNotNull(authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken());
    }

    @Test
    public void logoutTest() {
        @NotNull final String token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        Assert.assertNotNull(authEndpoint.logout(new UserLogoutRequest(token)));
    }

    @Test
    public void getProfileTest() {
        @NotNull final String token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        Assert.assertNotNull(authEndpoint.getProfile(new UserProfileRequest(token)).getUser());
    }

}
