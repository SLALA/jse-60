package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.configuration.ClientConfiguration;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.TaskListResponse;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.marker.IntegrationCategory;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskEndpointTest {

    @NotNull
    private static final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private static final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @NotNull
    private static final ITaskEndpoint taskEndpoint = context.getBean(ITaskEndpoint.class);

    @NotNull
    private static final IProjectEndpoint projectEndpoint = context.getBean(IProjectEndpoint.class);

    @Nullable
    private String token;

    @Before
    public void before() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr2", "pr2"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr1", "pr1"));
    }

    @After
    public void after() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        authEndpoint.logout(new UserLogoutRequest());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listTasksTest() {
        Assert.assertEquals(2, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listSortedTasksTest() {
        @NotNull final List<TaskDTO> list = taskEndpoint.listSortedTask(new TaskListSortedRequest(token, SortType.NAME.name())).getList();
        Assert.assertEquals(2, list.size());
        @NotNull final Comparator<TaskDTO> comparator = SortType.NAME.getComparator();
        Assert.assertEquals(list, list.stream().sorted(comparator).collect(Collectors.toList()));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void createTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr2", "pr2"));
        Assert.assertEquals(size + 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void clearTaskTest() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        TaskListResponse response = taskEndpoint.listTask(new TaskListRequest(token));
        Assert.assertEquals(0, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByIdTaskTest() {
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        @NotNull final TaskDTO actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals(expectedTask.getId(), actualTask.getId());
        Assert.assertEquals(expectedTask.getName(), actualTask.getName());
        Assert.assertEquals(expectedTask.getCreated(), actualTask.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByNameTaskTest() {
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        @NotNull final TaskDTO actualTask = taskEndpoint.findByNameTask(new TaskFindByNameRequest(token, expectedTask.getName())).getTask();
        Assert.assertEquals(expectedTask.getId(), actualTask.getId());
        Assert.assertEquals(expectedTask.getName(), actualTask.getName());
        Assert.assertEquals(expectedTask.getCreated(), actualTask.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByIdTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.removeByIdTask(new TaskRemoveByIdRequest(token, expectedTask.getId()));
        Assert.assertEquals(size - 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(token)).getList().contains(expectedTask));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByNameTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.removeByNameTask(new TaskRemoveByNameRequest(token, expectedTask.getName()));
        Assert.assertEquals(size - 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(token)).getList().contains(expectedTask));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByIdTaskTest() {
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.updateByIdTask(new TaskUpdateByIdRequest(token, expectedTask.getId(), "newName", "newDescr"));
        @NotNull final TaskDTO actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals("newName", actualTask.getName());
        Assert.assertEquals("newDescr", actualTask.getDescription());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByNameTaskTest() {
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.updateByNameTask(new TaskUpdateByNameRequest(token, expectedTask.getName(), "newName", "newDescr"));
        @NotNull final TaskDTO actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals("newName", actualTask.getName());
        Assert.assertEquals("newDescr", actualTask.getDescription());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void bindUnbindTaskToProjectTest() {
        @NotNull final ProjectDTO project = projectEndpoint.createProject(new ProjectCreateRequest(token, "pr1", "pr1")).getProject();
        @NotNull final TaskDTO expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        Assert.assertNull(expectedTask.getProjectId());
        taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, expectedTask.getId(), project.getId()));
        Assert.assertEquals(project.getId(), taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask().getProjectId());
        taskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(token, expectedTask.getId()));
        Assert.assertNull(taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask().getProjectId());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listByProjectIdTaskTest() {
        @NotNull final ProjectDTO project = projectEndpoint.createProject(new ProjectCreateRequest(token, "pr1", "pr1")).getProject();
        @NotNull final List<TaskDTO> list = taskEndpoint.listTask(new TaskListRequest(token)).getList();
        Assert.assertEquals(0, taskEndpoint.listByProjectIdTask(new TaskListByProjectIdRequest(token, project.getId())).getList().size());
        for (@NotNull TaskDTO task : list) {
            taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, task.getId(), project.getId()));
        }
        Assert.assertEquals(list.size(), taskEndpoint.listByProjectIdTask(new TaskListByProjectIdRequest(token, project.getId())).getList().size());
    }

}
