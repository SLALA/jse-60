package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.TaskDTO;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public AbstractTaskResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

}
