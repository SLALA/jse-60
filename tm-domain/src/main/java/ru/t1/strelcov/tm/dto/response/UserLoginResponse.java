package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @NotNull
    private String token;

    public UserLoginResponse(@NotNull final String token) {
        this.token = token;
    }
}
