package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;
import java.util.Vector;

@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_project")
@Entity
@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity {

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    public List<Task> tasks = new Vector<>();

    public Project(@NotNull User user, @NotNull String name) {
        super(user, name);
    }

    public Project(@NotNull User user, @NotNull String name, @Nullable String description) {
        super(user, name, description);
    }

}
