package ru.t1.strelcov.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.IPropertyService;

@Getter
@PropertySource("classpath:application.properties")
@Service
public class PropertyService implements IPropertyService {

    @Value("${mq.connection.factory:tcp://localhost:61616}")
    public String MQConnectionFactory;

    @Value("${mongo.host:localhost}")
    public String mongoHost;

    @Value("${mongo.port:27017}")
    public Integer mongoPort;

}