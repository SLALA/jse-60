package ru.t1.strelcov.tm.configuration;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.strelcov.tm.api.service.IPropertyService;

@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@ComponentScan("ru.t1.strelcov.tm")
public class ServerConfiguration {

    @SneakyThrows
    @Bean(initMethod = "start", destroyMethod = "stop")
    public BrokerService broker(@NotNull final IPropertyService propertyService) {
        BasicConfigurator.configure();
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector(propertyService.getMQConnectionFactory());
        return broker;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull @Value("${persistence.unit}") final String persistenceUnitName
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPersistenceUnitName(persistenceUnitName);
        factoryBean.setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
