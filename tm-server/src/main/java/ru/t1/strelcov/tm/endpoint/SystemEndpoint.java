package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint")
@NoArgsConstructor
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Autowired
    @NotNull
    private IPropertyService propertyService;

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getEmail());
        response.setName(propertyService.getName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getVersion());
        return response;
    }

}
