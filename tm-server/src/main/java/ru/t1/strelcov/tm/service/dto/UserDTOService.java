package ru.t1.strelcov.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.entity.UserAdminLockException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.repository.dto.UserDTORepository;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

@Getter
@Service
public class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @Autowired
    private UserDTORepository repository;

    @Autowired
    @NotNull
    protected IPropertyService propertyService;

    @Transactional
    @NotNull
    @Override
    public UserDTO add(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final UserDTO user = new UserDTO(login, passwordHash);
        add(user);
        return user;
    }

    @Transactional
    @NotNull
    @Override
    public UserDTO add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, email);
        add(user);
        return user;
    }

    @Transactional
    @NotNull
    @Override
    public UserDTO add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final UserDTO user;
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user = new UserDTO(login, passwordHash, role);
        add(user);
        return user;
    }

    @SneakyThrows
    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.removeByLogin(login)).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public UserDTO updateById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        userRepository.update(user);
        return user;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public UserDTO updateByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        userRepository.update(user);
        return user;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void changePasswordById(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user.setPasswordHash(passwordHash);
        userRepository.update(user);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.getRole() == Role.ADMIN) throw new UserAdminLockException();
        user.setLock(true);
        userRepository.update(user);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLock(false);
        userRepository.update(user);
    }

}
