package ru.t1.strelcov.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.entity.UserAdminLockException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.UserRepository;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

@Getter
@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    @NotNull
    protected IPropertyService propertyService;

    @Transactional
    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @Transactional
    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @Transactional
    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user;
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @SneakyThrows
    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.removeByLogin(login)).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public User updateById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        userRepository.update(user);
        return user;
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public User updateByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        userRepository.update(user);
        return user;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void changePasswordById(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user.setPasswordHash(passwordHash);
        userRepository.update(user);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.getRole() == Role.ADMIN) throw new UserAdminLockException();
        user.setLock(true);
        userRepository.update(user);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLock(false);
        userRepository.update(user);
    }

}
